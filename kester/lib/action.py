"""
Stuff to characterize and match to an action
"""

from map_range import map_range

resources = {} # things our Pattern's need to use, like midi

sofar = [] # the sequence we've seen so far
max_sofar = 5 # how many to keep

def characterize_accel_z( accel_smooth_z, accel_long_z, accel_slope_z_direction ):
    """
    Describe the action as direction and hard-hard, e.g.
    The slope of an acceleration is jerk
    degree/jerk is 0,1,2
        direction, degree = characterize_accel_z( accel_smooth_z, accel_long_z, accel_slope_z )
    """
    return (accel_slope_z_direction, 1)

reached_string = False # state
plucked = False # state
def characterize_wrist_bend( pinch, bent, accel):
    """
    We want to know things like: how far before reversing, how fast on the reverse.
    Fortunately, the bend sensor is absolute.
    """
    global reached_string,plucked # fixme: object, because we have state

    # we only care during pinch'd
    if not pinch:
        return [None,0]

    # bend down increases the resistance from the base
    # bend up decreases resistance!
    zero = 48500
    string_dist = 3000 # how far a bend would reach the string?
    pluck_dist = string_dist + 250 # where, on the movement, does the string pluck?

    # States (while pinch'd and moving):
    # accel -> and less than string: "not pluck'd yet"
    # not pluck'd and accel -> and > pluck-point: "pluck", pluck'd
    # and only send "pluck'd" once
    # AND reverse -> & < for other direction

    send_note = False

    if accel > 0:
        # moving toward the string
        if bent < (zero + string_dist):
            # detect (reset) "not pluck'd yet"
            reached_string = False
            plucked = False
            #print(f"Reset -> {bent} {accel} z{zero} s{zero+string_dist} p{zero+pluck_dist}")
        elif not reached_string and bent >= (zero + string_dist):
            # detect "picked up string"
            reached_string = True
            plucked = False
            print(f"Picked up -> {bent}")
        elif reached_string and not plucked and bent >= (zero + pluck_dist):
            # pluck
            # this also inhibits future "pluck" detection, until direction reverses
            plucked = True
            send_note = True # only the once!
            print(f"plucked -> {bent}")
    elif accel < 0:
        # moving toward the string
        if bent > (zero + string_dist):
            # detect (reset) "not pluck'd yet"
            reached_string = False
            plucked = False
            #print(f"Reset <- {bent} {accel} z{zero} s{zero+string_dist} p{zero+pluck_dist}")
        elif not reached_string and bent <= (zero + string_dist):
            # detect "picked up string"
            reached_string = True
            plucked = False
        elif reached_string and not plucked and bent <= (zero + pluck_dist):
            # pluck
            # this also inhibits future "pluck" detection, until direction reverses
            plucked = True
            send_note = True # only the once!

    if send_note:
        # midi velocity is 0..127, bend acceleration is 0..30
        midi_velocity = int(map_range(abs(accel), 0,2500, 0,127))
        print(f"Pluck {accel}")
        return [1, midi_velocity]

    # something else
    return [None,0]

def which_wrist_action( accel_dir, midi_velocity ):
    """
    `wrist_action` from characterize_wrist_bend()
        [ accel-direction, midi-velocity ]
    find a pattern and return it, e.g.
    pattern = which_wrist_action( *wrist_action )
    pattern.action()
    """


    if accel_dir == 1: # up
            return PatternPluck(velocity=midi_velocity)
    else:
        return Pattern() # i.e no action

def match_template(tmpl):
    sofar_offset = - len(tmpl)
    if len(sofar) < len(tmpl):
        return False
    x = sofar[ sofar_offset: ]
    #print(f"{ x } vs {tmpl}")
    for i,templ_v in enumerate(tmpl):
        if templ_v != sofar[sofar_offset + i]:
            return False
    return True

def which_accel_z_pattern( direction, degree ):
    """
    find a pattern and return it, e.g.
    pattern = which_accel_z_pattern( direction, degree )
    pattern.action()
    """

    # We need to accumulate meaningful patterns, like
    # was accel down deg=1,
    sofar.append( (direction, degree) )
    # only keep n
    if len(sofar) > max_sofar:
        sofar.pop(0)

    # if we are slow at sampling, we'll miss the '0', so allow alternate of down->up
    pluck = [ (-1,1), (-1,1), (0,1) ] # down,down,zero
    pluck2 = [ (-1,1), (-1,1), (1,1) ] # down,down,up

    if match_template(pluck) or match_template(pluck2):
            return PatternPluck()
    else:
        return Pattern() # i.e no action

class Pattern:
    def __init__(self):
        pass

    def action(self):
        """do something useful"""
        pass

from note_on import NoteOn,NoteOff
class PatternPluck(Pattern):
    def __init__(self, note=50, velocity=127):
        super().__init__()
        self.velocity = velocity
        self.note = note

    def action(self):
        print(f"### Pluck {self.note}^{self.velocity}")
        if 'midi' in resources:
            resources['midi'].write( bytes(NoteOn(self.note, velocity=self.velocity)) )

