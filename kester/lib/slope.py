class Slope(object):
  # a simple slope tracker

  def __init__(self, initial=0, slope_noise=0):
    # these need to be float math
    self._last = initial
    self._slope = 0
    self._slope_noise = slope_noise

  @property
  def value(self):
    return self._slope

  def reset(self, v):
    self._last = v
    self.slope = 0
    return v

  @property
  def direction(self):
    if self._slope > self._slope_noise:
        return 1
    elif self._slope < - self._slope_noise:
        return -1
    else:
        return 0

  # we intend it to inline
  def update(self, raw_value):
    self._slope = raw_value - self._last
    self._last = raw_value
    return self.value
