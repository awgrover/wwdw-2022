def map_range(x, in_min, in_max, out_min, out_max):
  rez = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  if rez < out_min:
    rez = out_min
  if rez > out_max:
    rez = out_max
  return rez
