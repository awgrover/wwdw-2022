''' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    MAPS - MICA Automated Pumping System
                          Memory monitoring module
                    Develpoed by Ryan Hoover via BWF CDP
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  '''
import gc
mem_show = True
mem_running = gc.mem_free()

def mem_stat(lbl):
    if mem_show:
        global mem_running
        current = gc.mem_free()
        load = (mem_running - current)
        gap=" " * (18 -len(lbl)-len(str(load)))
        print(f'MEM> {lbl}:{gap} {load} \t Current: {current}')
        mem_running = gc.mem_free()
