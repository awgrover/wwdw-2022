"""
remove the parser stuff to make it smaller. modified from Adafruit_CircuitPython_MIDI. by Alan Grover
usage now:
    
"""

# SPDX-FileCopyrightText: 2019 Kevin J. Walters for Adafruit Industries
#
# SPDX-License-Identifier: MIT

"""
`adafruit_midi.note_on`
================================================================================

Note On Change MIDI message.


* Author(s): Kevin J. Walters

Implementation Notes
--------------------

"""

__version__ = "1.4.5"


class NoteOn:
    """Note On Change MIDI message.

    :param note: The note (key) number either as an ``int`` (0-127)
    :param int velocity: The strike velocity, 0-127, 0 is equivalent
        to a Note Off, defaults to 127.
    """

    CHANNELMASK = 0x0F
    _STATUS = 0x90
    _STATUSMASK = 0xF0
    LENGTH = 3

    def __init__(self, note, velocity=127, channel=1):
        self.note = note
        self.velocity = velocity
        self.channel = channel
        if not 0 <= self.note <= 127 or not 0 <= self.velocity <= 127:
            raise ValueError(f"Out of range, note 0..127, saw {note} vel {velocity}")

    @property
    def channel(self):
        """The channel number of the MIDI message where appropriate.
        This is *updated* by MIDI.send() method.
        """
        return self._channel

    @channel.setter
    def channel(self, channel):
        if channel is not None and not 0 <= channel <= 15:
            raise ValueError("Channel must be 0-15 or None")
        self._channel = channel

    def __bytes__(self):
        """handles bytes(thisobject)"""
        return bytes(
            [self._STATUS | (self.channel & self.CHANNELMASK), self.note, self.velocity]
        )

    @classmethod
    def from_bytes(cls, msg_bytes):
        return cls(msg_bytes[1], msg_bytes[2], channel=msg_bytes[0] & cls.CHANNELMASK)

class NoteOff(NoteOn):
    def __init__(self, note, channel=1):
        super().__init__(note,velocity=0,channel=channel)
