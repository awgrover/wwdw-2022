"""
    Bend sensor(s)
    IMU's: BN055
        https://docs.circuitpython.org/projects/bno055/en/latest/api.html
        datasheet: https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf
    MIDI
        piano lowest  note is midi 21
              highest note is midi 88
    https://computermusicresource.com/midikeys.html
    https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies

    Calibrate:
        hold still, preferably flat
        all sides of a cube! (slow between)
        figure 8
"""
import gc
from mem import mem_stat
mem_stat('start')

import time,sys
from math import sqrt
import board,analogio,digitalio
import busio
import supervisor
import usb_midi
mem_stat('system')

from note_on import NoteOn,NoteOff
gc.collect()
mem_stat('midi')

from exponential_smooth import ExponentialSmooth
from slope import Slope
import action
from every import Every

USEIMU=True

if USEIMU:
    import adafruit_bno055
    i2c = busio.I2C(board.SCL, board.SDA)
    imu = adafruit_bno055.BNO055_I2C(i2c)
    """
    # bah, you can't set these if already in mode= NDOF_MODE (fusion), the default.
    # these should be the defaults
    imu.mode = adafruit_bno055.NDOF_MODE # absolute fusion
    imu.accel_range = adafruit_bno055.ACCEL_2G # not "2"
    imu.accel_bandwidth = adafruit_bno055.ACCEL_62_5HZ # sadly, not "62.5"
    imu.gyro_bandwidth = adafruit_bno055.GYRO_32HZ
    imu.gyro_range = adafruit_bno055.GYRO_2000_DPS # 1000,500,250
    imu.magnet_rate = adafruit_bno055.MAGNET_20HZ
    """
    IMUBandwidth = max(62.5,32,20) * 2 # Hz, samples/sec keep in sync with the above

    DoIMUSample = Every( 1 / IMUBandwidth ) # when do we sample
    print(f"IMU {imu.__class__.__name__} mode {imu.mode} sample {IMUBandwidth} Hz {1/IMUBandwidth} secs")
    for p in ('accel_range','accel_bandwidth','gyro_bandwidth','gyro_range','magnet_rate'):
        print(f"  {p}= {getattr(imu,p)}")

    # for .linear_acceleration
    accel_noise = { 'x':0.1 * 1.1, 'y':0.1 * 1.1, 'z':0.1 * 1.1 } # empirical
    accel_noise['xy'] = sqrt(accel_noise['x'] * accel_noise['x'] + accel_noise['y'] * accel_noise['y'] )
else:
    imu = None
    DoIMUSample = Every( 1 ) # dumy sample

bend_wrist_sensor = analogio.AnalogIn(board.A1)

pinch = digitalio.DigitalInOut(board.A2)
pinch.switch_to_input(pull=digitalio.Pull.UP) # reversed for short-circuit safety: open=True, pinch=False

midi_out = usb_midi.ports[1]
midi_piano = { 'highest' : 108, 'lowest' : 21, 'middlec' : 60, 'count' : 88 }
action.resources['midi'] = midi_out # let the action `PatternPluck` know about midi


def other_actions():
    """Do other things, like..."""

    # hopefully a full line:
    if not supervisor.runtime.serial_bytes_available:
        return

    command = input()

    if f"command_{command}" in globals():
        f = globals()[ f"command_{command}" ]
        print(f"> {command}")
        f()
        print("<")
    elif command == '':
        pass
    else:
        if command != '?':
            print("no such")
        print( list(x for x in globals().keys() if x.startswith('command_')) )
        print("'?' for list of commands")
        print("Command or return to continue")
        while not supervisor.runtime.serial_bytes_available:
            pass

def xyz_str(xyz):
        return ' '.join(str(xyz[k]) for k in ('x','y','z'))
def xyz_sub(a,b):
    """subtract b from a"""
    res = {}
    for k in a.keys():
        res[k] = a[k] - b[k]
    return res

def command_p():
    """pause"""
    print("Pausing, return to continue")
    while not supervisor.runtime.serial_bytes_available:
        pass
    input()

def command_noise():
    """try to figure out the noise"""

    max_noise = {'x':0,'y':0,'z':0}
    min_noise = {'x':1e9,'y':1e9,'z':1e9}
    lnoise = {'x':0,'y':0,'z':0}
    center = {}
    while not supervisor.runtime.serial_bytes_available:
        accel = {}
        accel['x'], accel['y'], accel['z'] = imu.acceleration
        changed = False
        for k in ('x','y','z'):
            max_noise[k] = max( max_noise[k], accel[k] )
            min_noise[k] = min( min_noise[k], accel[k] )
            center[k] = (max_noise[k] + min_noise[k] ) / 2
            changed |= max_noise[k] != lnoise[k]
            lnoise[k] = max_noise[k]
        print( ' '.join( (xyz_str(accel),xyz_str(xyz_sub(max_noise,center)),xyz_str(xyz_sub(center,min_noise))) ) )

def command_note():
    """test midi out"""
    midi_out.write( bytes(NoteOn(50)) )
    time.sleep(0.3);
    midi_out.write( bytes(NoteOff(50)) )
    for x in range(1,3):
        midi_out.write( bytes(NoteOn(80)) )
        time.sleep(0.1);
        midi_out.write( bytes(NoteOff(80)) )

def command_wbend():
    """show wrist bend values"""

    bent_wrist_last = 0
    while not supervisor.runtime.serial_bytes_available:
        bent_wrist = bend_wrist_sensor.value
        # figure the accel, smooth it, and determine the slope of it
        bent_wrist_accel = bent_wrist - bent_wrist_last # this slope==this acceleration
        bent_wrist_slope.update( bent_wrist_accel )
        bent_wrist_last = bent_wrist
        #print(f"( {bent_wrist}, {bent_wrist_slope.value}, {bent_wrist_slope.direction} )")
        print(f"( {bent_wrist} )")
        time.sleep(0.05)

def command_r():
    global position,hand_pos
    position = 0
    hand_pos = 0

def constrain(v,small,large):
    return min(max(v,small),large)

def wait_for_calibration(volume):
    """Wait for imu to be ready, play notes"""
    update = Every(0.1)
    sys_calib = 0
    
    # sys_calib is "all sensors"
    # empirically, I get unstable 2|3 for sys_calib
    while sys_calib < 2:
        if update():
            sofar = imu.calibration_status # (sys, gyro, accel, mag ) 0==not, 1==barely, 3==good
            #print(sofar)
            sys_calib = sofar[0]
            # sort of converging on middle
            state = sum(sofar) * 3 # "12" when all are 1
            midi_out.write( bytes(NoteOn(midi_piano['lowest'] + state, velocity=volume)) )
            midi_out.write( bytes(NoteOn(midi_piano['highest'] - state, velocity=volume)) )

    # all C's
    for n_c in range(24,120,12):
        midi_out.write( bytes(NoteOn(n_c, velocity=volume)) )


print("start")
progress_volume = 30
midi_out.write( bytes(NoteOn(midi_piano['middlec'], velocity=progress_volume)) )

wait_for_calibration(progress_volume)
print(f"IMU calibrated {imu.calibration_status}")

if imu:
    accel_x, accel_y, accel_z = imu.acceleration
    accel_smooth_z = ExponentialSmooth(5, accel_z)
    accel_long_z = ExponentialSmooth(10, accel_z)
    accel_slope_z = Slope(accel_z, accel_noise['z'] * 1.1)

bent_wrist_last = 0
bent_wrist_slope = Slope( 0 ) # no noise, I think


string_gap = 0.01 # meters
string_position = { 'min' : 0, 'max' : 11 } # string number range

guzheng = ( # the midi note for each string
    48, 50, 52, 53, 55, 57, 59,
    60, 62, 64, 65, 67
    )

last_pinch = False
hand_pos = 0
last_hand_pos = -1
position = 0 # accumulate position
speed = 0 # accumulate
last_sample_time = time.monotonic_ns()
last_dir_x = 0

while True:
    # allow us to type stuff to test things. Try '?' for list
    other_actions()

    #
    # Do some stuff with the bend
    #

    bent_wrist = bend_wrist_sensor.value
    # figure the accel, smooth it, and determine the slope of it
    bent_wrist_accel = bent_wrist - bent_wrist_last # this slope==this acceleration
    # noise at 64/32
    if abs(bent_wrist_accel) <= 64:
        bent_wrist_accel = 0
    #bent_wrist_slope.update( bent_wrist_accel )
    #if bent_wrist_last != bent_wrist and abs(bent_wrist_accel > 32):
    #    print(f"{bent_wrist} {bent_wrist_accel}")
    bent_wrist_last = bent_wrist

    # strum is a bend that crosses the string
    wrist_action = action.characterize_wrist_bend(not pinch.value, bent_wrist, bent_wrist_accel )
    pattern = action.which_wrist_action( *wrist_action )
    pattern.action()

    #
    # Which string are we "at"?
    #
    # IMU has too much drift over time, but it is reasonably fast. Use it w/re-zero'ing
    # use slower lidar to re-zero, but exactly what geometry? lidar on belt? lidar on hand?
    # IMU does have abs orientation, could do trig for distance to body
    # or ... more bend sensors on elbow, shoulder?

    #
    # Do some stuff with the accelerometers
    #
    if imu and DoIMUSample():
        # FIXME: move to an "action"
        accel = {}
        accel['x'], accel['y'], accel['z'] = imu.linear_acceleration
        now = time.monotonic_ns()
        speed = 0 # for printing
        d_pos = 0 # for printing
        dir = {'x':0}

        # more than noise, an attempt to prevent drift
        accel_magnitude = sqrt(accel['x'] * accel['x'] + accel['y'] * accel['y'] )
        if position > 0 or accel_magnitude > accel_noise['xy']: # abs(accel['x']) > accel_noise['x'] and abs(accel['y']) > accel_noise['y']:
            # we need the sign for the adding, and it's never 0 here
            dir = {}
            dir['x'] = 1 if accel['x'] > 0 else -1
            dir['y'] = 1 if accel['y'] > 0 else -1
            # the "axis" of motion we care about is: away/towards, and the imu is upside down
            # so: -x,+y or +x,-y
            if dir['x'] != dir['y']:
                d_time = (now - last_sample_time) / 1e9
                speed += accel_magnitude * d_time * dir['x'] # m/s
                # don't forget direction!
                d_pos = speed * d_time
                position += d_pos # meters

                # finished accelerating!
                # quantize position
                # we aren't sampling fast enough to see the 0 crossing
                # if accel_magnitude <= accel_noise['xy']:
                if dir['x'] != last_dir_x:
                    hand_pos = constrain( int(position / string_gap ), string_position['min'],string_position['max'] )
                    position = 0 # flag for moving vs done-moving, and accumulator
                    speed = 0
                    last_dir_x = dir['x']


        last_sample_time = now
        #sys.stdout.write(" ".join(list(str(x) for x in accel.values())))
        #sys.stdout.write(" ")
        sys.stdout.write(" ".join(list(str(accel[x]) for x in ('x','y','z'))))
        sys.stdout.write(" ")
        sys.stdout.write(f" {accel_magnitude * dir['x']} {speed}  {d_pos}   {hand_pos} {position}")
        print()

        # debug string pos
        if hand_pos != last_hand_pos:
            midi_out.write( bytes(NoteOn(midi_piano['middlec'] + hand_pos, velocity=127)) )
            last_hand_pos = hand_pos

        """
        accel_x, accel_y, accel_z = imu.acceleration

        # smooth z to get rid of some noise.
        accel_smooth_z.update( accel_z )
        # "reference" position, i.e. slower response
        accel_long_z.update( accel_z )
        # to detect end of acceleration
        accel_slope_z.update( accel_z )

        # pluck is an accelerate down (not so hard), then harder accel up
        direction, degree = action.characterize_accel_z( accel_smooth_z.value, accel_long_z.value, accel_slope_z.direction )
        pattern = action.which_accel_z_pattern( direction, degree )
        #pattern.action() # disabled for the moment
        """

    #
    # Debug
    #

    if not USEIMU:
        #mag_x, mag_y, mag_z = imu.magnetic
        #print(f'({bent_wrist}, {mag_x}, {mag_y}, {mag_z})')
        #gyro_x, gyro_y, gyro_z = imu.gyro
        #temp = imu.temperature
        pass

    #print(f'( {bent_wrist}, {accel_x}, {accel_y}, {accel_smooth_z.value}, {accel_long_z.value} )')
    #print(f'( {accel_smooth_z.value}, {accel_long_z.value}, {accel_slope_z.value}, { accel_slope_z.direction } )')
    #print(f'({bent_wrist})')

    # Stupid test of pinch: play a note
    """
    if not pinch.value: # means 'pinch' because reversed
        if last_pinch == False: # i.e. changed to pinch
            print("pinch")
            midi_out.write( bytes(NoteOn(guzheng[hand_pos], velocity=127)) )
        last_pinch = True
    else:
        last_pinch = False
    """

    time.sleep(0.02)
