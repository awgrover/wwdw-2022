"""
    NOTE: if you see the backlight turn on and off, then communication is working.
    If you don't see characters on the display, then tweak the contrast untill you
    do!
"""

from time import sleep
import board, busio
import adafruit_character_lcd.character_lcd_i2c as character_lcd # https://docs.circuitpython.org/projects/charlcd/en/latest/
i2c = busio.I2C(board.SCL, board.SDA)
cols = 16
rows = 2
lcd = character_lcd.Character_LCD_I2C(i2c, cols, rows,address=0x20)
lcd.display = True
lcd.backlight = True
lcd.clear()

ct = 0
while True:
    msg = ((str(ct) * cols) + "\n") * rows
    print(msg)
    lcd.message = msg
    ct = (ct + 1) % 10
    sleep(1)
    for x in range(1,5):
        lcd.backlight = not lcd.backlight
        sleep(0.2)
    lcd.clear()
