import sys
import adafruit_dht
import board
from every import Every

print("Make dht...")
dht = dht_device = adafruit_dht.DHT11(board.D5)

do_dht = Every(1) # fastest allowed!

temperature = 0
humidity = 0.0
dht_error_count = 0

print("Read every",do_dht.interval)
while True:
    if do_dht():
        sys.stdout.write('.')
        sys.stdout.flush()
        try:
            temperature = dht.temperature
            humidity = dht.humidity
            print(f"Humidity {humidity} Temp {temperature}")
        except Exception as e:
            print(dht_error_count,": ",str(e))
            dht_error_count += 1
