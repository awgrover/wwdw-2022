"""
The LDR is just on an ADC pin.
As is the moisture sensor.
"""

from analogio import AnalogIn
from every import Every

ldr = AnalogIn(board.A1)
moisture = AnalogIn(board.A2)
say = Every(0.3)

while True:
    if say():
        print( f"Light: {ldr.value} Moisture {moisture.value}" );

