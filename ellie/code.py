#!/usr/bin/python3
"""
    Tries to follow
    https://www.hackster.io/pohwl/ecoplant-plant-monitoring-5e6767
    BUT
        Now using circuitpython

    # summary of setup
        # from adafruit-circuitpython-bundle
        # adafruit_dht
        # adafruit_character_lcd # https://docs.circuitpython.org/projects/charlcd/en/latest/

    Try ./test-ldr-moisture.py as a simple test
    And the other test-*...

    uses a lcd with lcd-backpack (i2c interface)
        Vcc : 5v
        DAT : SDA
        CLK : SCL
    Note: you must tweak the "contrast" to get characters to show, otherwise it may look like it doesn't work.

    Try
        test-i2c-scan.py
    to find the address.
    NOTE: the number is hex, so '20' needs to be written as 0x20 below, in the 'LCD(...)'.

    uses an adafruit mosisture sensor
        SIG : A1
        using GND and +3

    has a light sensor (ldr) 
        A0

    uses a DHT11 temp/humidity sensor
        D5 w/pull-up
        also needs GND and +3
"""
import os
# all file paths should be relative to "this" directory
here = os.path.dirname(os.path.realpath(__file__)) # our directory
os.chdir(here)

import sys
import json
import datetime as datetime
from time import sleep

import board,busio

import adafruit_dht
import adafruit_character_lcd.character_lcd_i2c as character_lcd # https://docs.circuitpython.org/projects/charlcd/en/latest/

from every import Every

redLed = DigitalInOut(1); redLed.direction = Direction.OUTPUT
greenLed = DigitalInOut(2); greenLed.direction = Direction.OUTPUT
whiteLed = DigitalInOut(3); whiteLed.direction = Direction.OUTPUT
motor = DigitalInOut(4); motor.direction = Direction.OUTPUT

motor.value = True

# turn 'em all on for show
greenLed.value = True
redLed.value = True
whiteLed.value = True

motor_modulate = Every(0.1, 0.1) # pattern of durations on-off...

ldr = AnalogIn(board.A1)
moisture = AnalogIn(board.A2)
do_ldr_moisture = Every(0.1)

i2c = busio.I2C(board.SCL, board.SDA)

lcd_cols = 16
lcd_rows = 2
lcd = character_lcd.Character_LCD_I2C(i2c, lcd_cols, lcd_rows,address=0x20)
lcd.backlight = True

dht = adafruit_dht.DHT11(board.D5)
do_dht = Every(1.1) # maximum read rate for DHT11
do_dht() # skip the immediate read

def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")

def lcd_line(msg, line_number):
    # convenience for line-by-line display
    # clears display on line_number==1

    if line_number == 1:
        lcd.clear()

    if line_number == 2:
        lcd.cursor_position(0,1)

    lcd.message = msg

# we only read config once
limit_data_file = 'plant.json'
with open(limit_data_file) as db:
    limitData = json.load(db)
print(limitData)

motor.value = False

do_message = Every(0.3)
errorTimes = 0
temperature = 0
humidity = 0.0
dht_error_count = 0

while True:
    try:

        fixme: console input to adjust things?

        allOk = True

        # reading the dht can fail
        if do_dht(): # max rate
            try:
                temperature = dht.temperature
                humidity = dht.humidity
                # why?
                if not humidity:
                    humidity = 0.0
                if humidity > 100:
                    humidity = humidity - 100

                dht_error_count = 0
            except RuntimeError as e:
                if "Checksum did not validate" in str(e) or "A full buffer was not returned" in str(e) or "Received unplausible data" in str(e) or "DHT sensor not found" in str(e):
                    dht_error_count += 1
                    if dht_error_count > 10:
                        # 10 in a row means something bad
                        print("Many errors in a row...")
                        raise e
                else:
                    # something else
                    raise e

        # we are getting None sometimes
        if not humidity:
            humidity = 0.0
        if not temperature:
            temperature = 0.0

        if do_ldr_moisture():
            moistureLevel = round(moisture.value*1024,2)
            light_value = round(ldr.value*1024,2)

        if temperature > float(limitData['tempLimit']) and humidity > float(limitData['humidityLimit']):
            redLed = ! redLed.value
            greenLed.value = False
            lcd_line('Temp & Humidity', 1)
            lcd_line('too high', 2)
            errorTimes += 1
            allOk = False
        elif temperature > float(limitData['tempLimit']):
            redLed = ! redLed.value
            greenLed.value = False
            lcd_line('Temperature', 1)
            lcd_line('too high', 2)
            errorTimes += 1
            allOk = False
        elif humidity > float(limitData['humidityLimit']):
            redLed = ! redLed.value
            greenLed.value = False
            lcd_line('Humidity', 1)
            lcd_line('too high', 2)
            errorTimes += 1
            allOk = False
        elif moistureLevel < float(limitData['moistureLimit']):
            redLed. = ! redLed.value
            greenLed.value = False
            lcd_line('Plant', 1)
            lcd_line('needs water', 2)
            errorTimes += 1
            allOk = False
        else:
            # "normal" message
            lcd_line('Temp: {} C'.format(temperature), 1)
            lcd_line('Humidity: {}%'.format(humidity), 2)


        motor_on_level = 300
        if moistureLevel > motor_on_level:
            if motor_modulate():
                motor.value = ! ! motor_modulate.i % 2
        elif moistureLevel < motor_on_level*0.9:
            motor.value = False # inverted

        if allOk:
            greenLed.value = True
            redLed.value = False
            errorTimes = 0
        else:
            if errorTimes == 1:
                if temperature > float(limitData['tempLimit']) and humidity > float(limitData['humidityLimit']):
                    print('`Plant temperature and humidity too high: ' + str(temperature) + '°C,' + str(humidity) + '%`')

                elif temperature > float(limitData['tempLimit']):
                    print('`Plant temperature too high: ' + str(temperature) + '°C`')
                elif humidity > float(limitData['humidityLimit']):
                    print('`Plant humidity too high: ' + str(humidity) + '%`')
                elif moistureLevel < float(limitData['moistureLimit']):
                    print('`Plant too dry.Requires watering`')
            sleep(1) # allow display to show

        # dark is higher values!
        if light_value < float(limitData['lightLimit']):
            whiteLed.value = True
        else:
            whiteLed.value = False

        if do_message():
            message = {}
            #message["id"] = "sensordata"
            message["datetime_value"] = str(datetime.datetime.now())
            message["temperature"] = temperature
            message["humidity"] = humidity
            message["lightValue"] = light_value
            message["moistureLevel"] = moistureLevel
            print(message)
    except KeyboardInterrupt:
        print("Program aborted")
        sys.exit()
