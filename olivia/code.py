"""
tentacle/collision prototype

When something is close, buzz.

Wiring:

    Circuitplayground express
    vl53l1x (timeofflight lidar)
        use a stemma pigtail
    vibrator motor
        use a 2n2222
        and ~1.5k resistor to gate

    cpe     vl53l1x     motor
    ---     -------     -----
    GND     GND         motor B
    Vout                trans #1
    A1                  1.5k resistor -> #2
                        trans #3 -> motor A
    (or use stemma for sda/scl)
    3V      VIN
    SDA     SDA
    SCL     SCL

    diode backwards across motor
    .1muF across motor
"""

import board
import digitalio
import busio

# https://docs.circuitpython.org/projects/vl53l0x/en/latest/
import adafruit_vl53l4cd

# setup the motor control pin
motor = digitalio.DigitalInOut(board.A1)
motor.switch_to_output()

# Initialize I2C bus and sensor.
i2c = busio.I2C(board.SCL, board.SDA)
vl53 = adafruit_vl53l4cd.VL53L4CD(i2c)

# Optionally adjust the measurement timing budget to change speed and accuracy.
# See the example here for more details:
#   https://github.com/pololu/vl53l0x-arduino/blob/master/examples/Single/Single.ino
# For example a higher speed but less accurate timing budget of 20ms:
# vl53.measurement_timing_budget = 20000
# Or a slower but more accurate timing budget of 200ms:
# vl53.measurement_timing_budget = 200000
# The dfault timing budget is 33ms, a good compromise of speed and accuracy.

last_distance = -1
def tof_distance():
    """ Distance or -1 if none yet """
    try:
        if vl53.data_ready:
            vl53.clear_interrupt()

            # .distance is cm, .range is mm
            distance_to_object = vl53.distance
            print(distance_to_object)
            return distance_to_object
        
    except OSError as e:
        if "Input/output error" in str(e):
            print("I/O err...")
        else:
            raise e
    return -1

vl53.start_ranging()

while True:

    # when we can get a measurement, turn on the motor if "close"
    distance_to_object = tof_distance()

    if distance_to_object != -1:
        if distance_to_object < 20:
            if motor.value == False:
                print("  ON")
            motor.value = True
        elif distance_to_object > 25:
            # Don't turn off at 20, or you will get rapid on/off/on/off
            # "hysteresis"
            if motor.value == True:
                print("  off")
            motor.value = False
